import React from "react";
import { useEffect, useState, useRef } from "react";
import { sleep, API } from "../utils";

const ComponentWithUseEffectCallAPI = () => {
  const [todoList, setTodoList] = useState([]);
  const isMounted = useRef(true);

  useEffect(() => {
    // Callback
    const getTodoList = async () => {
      const response = await API.fetchData();

      await sleep(1000);
      console.log("finished call API");

      // Update state
      if (isMounted.current) {
        console.log("setUsers");
        setTodoList(response);
      }
    };

    getTodoList();

    // Clean up
    return () => {
      console.log("unMounted");
      isMounted.current = false;
    };
  }, []);

  return (
    <div>
      {todoList.map((item) => (
        <div key={item.id}>
          <p>{`${item.id}: ${item.title}`}</p>
        </div>
      ))}
    </div>
  );
};

export default ComponentWithUseEffectCallAPI;
