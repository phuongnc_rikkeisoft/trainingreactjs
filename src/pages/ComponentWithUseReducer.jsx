import React, { useReducer, useEffect } from "react";
import {
  systemAction,
  systemReducer,
  SYSTEM_INITIAL_STATE,
  todoAction,
  todoReducer,
  TODO_INITIAL_STATE,
} from "../store";
import { sleep, API, logger } from "../utils";
import PageContainer from "../layouts/PageContainer";

const ComponentWithUseReducer = () => {
  const [systemState, systemDispatch] = useReducer(logger(systemReducer), SYSTEM_INITIAL_STATE);
  const [todoState, todoDispatch] = useReducer(logger(todoReducer), TODO_INITIAL_STATE);
  const { isLoading } = systemState;
  const { todoList } = todoState;

  useEffect(() => {
    // Start loading
    systemDispatch({
      type: systemAction.SET_LOADING,
      payload: true,
    });

    const getTodoList = async () => {
      try {
        const response = await API.fetchData();
        // Delay 3s
        await sleep(3000);
        // Update state
        if (response?.length) {
          todoDispatch({
            type: todoAction.SET_TODO_LIST,
            payload: response,
          });
        }
      } catch (e) {
        console.log("Error: ", e);
      } finally {
        // End loading
        systemDispatch({
          type: systemAction.SET_LOADING,
          payload: false,
        });
      }
    };

    // Call api
    getTodoList();
  }, []);

  return (
    <PageContainer isLoading={isLoading}>
      <div>
        {todoList.map((item) => (
          <div key={item.id}>
            <p>{`${item.id}: ${item.title}`}</p>
          </div>
        ))}
      </div>
    </PageContainer>
  );
};

export default ComponentWithUseReducer;
