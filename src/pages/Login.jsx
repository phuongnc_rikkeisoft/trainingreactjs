import React from "react";
import AuthContext from "../context/AuthContext";
import { systemAction } from "../store/actions";

const Login = () => {
  return (
    <AuthContext.Consumer>
      {(context) => {
        return (
          <form>
            <div>
              <label htmlFor="">Username: </label>
              <input type="text" />
            </div>
            <div>
              <label htmlFor="">Password: </label>
              <input type="text" />
            </div>
            <div>
              <button type="button" onClick={() => context.dispatch({ type: systemAction.SET_LOGIN })}>
                Login
              </button>
            </div>
          </form>
        );
      }}
    </AuthContext.Consumer>
  );
};

export default Login;
