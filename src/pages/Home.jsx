import React from "react";
import { useAuthContext } from "../context/AuthContext";
import { systemAction } from "../store/actions";

const HomePage = () => {
  const { dispatch } = useAuthContext();

  return (
    <div>
      <h1>Home page</h1>
      <button onClick={() => dispatch({ type: systemAction.SET_LOGOUT })}>Logout</button>
    </div>
  );
};

export default HomePage;
