import React from "react";
import { useAuthContext } from "./context/AuthContext";
import HomePage from "./pages/Home";
import Login from "./pages/Login";

const App = () => {
  const { state } = useAuthContext();

  return <div className="App">{state.isLogged ? <HomePage /> : <Login />}</div>;
};

export default App;
