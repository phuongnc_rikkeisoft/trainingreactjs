import { createContext, useContext, useReducer } from "react";
import { systemReducer, SYSTEM_INITIAL_STATE } from "../store/reducers";
import logger from "../utils/logger";

const AuthContext = createContext(SYSTEM_INITIAL_STATE);

export const AuthContextProvider = ({ children }) => {
  const [systemState, dispatch] = useReducer(logger(systemReducer), SYSTEM_INITIAL_STATE);
  const value = {
    state: systemState,
    dispatch,
  };

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};

export const useAuthContext = () => useContext(AuthContext);

export default AuthContext;
